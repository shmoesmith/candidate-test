package com.alan.vincent;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.NoSuchFileException;
import java.util.stream.Collectors;
import java.lang.StringBuilder;

public class Main {

    public static void main(String[] args) {

        try {
            List<Award> objects = Files.readAllLines(Paths.get(Main.class.getClassLoader().getResource("academy_award_actresses.csv").toURI()))
                    .stream()
                    .map(s -> new Award(s.split(",")[0], s.split(",")[1], s.split(",")[2]))
                    .collect(Collectors.toList());
            //remove headers
            objects.remove(0);
            StringBuilder output = new StringBuilder("[");
            for(Award award:objects) {
                output.append(award.toString());
                output.append(",");
            }
            output.deleteCharAt(output.length()-1);
            output.append("]");
            System.out.println(output);


        } catch (NoSuchFileException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

    }

    }

