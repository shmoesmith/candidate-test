package com.alan.vincent;

import java.lang.StringBuilder;

public class Award {
    private String year;
    private String actress;
    private String movie;

    public Award(String year, String actress, String movie) {
        this.year = year;
        this.actress = actress;
        this.movie = movie;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getActress() {
        return actress;
    }

    public void setActress(String actress) {
        this.actress = actress;
    }

    public String getMovie() {
        return movie;
    }

    public void setMovie(String movie) {
        this.movie = movie;
    }

    public String toString() {
        StringBuilder output = new StringBuilder("{");
        output.append("\"year\":").append(this.year).append(",");
        output.append("\"actress\":").append(this.actress).append(",");
        output.append("\"movie\":").append(this.movie);
        output.append("}");
        return output.toString();
    }
}
